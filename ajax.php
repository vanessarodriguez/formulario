<?php 
	// include autoload.php
    require dirname(__FILE__) . '/autoload.php';	

    // consulta de sujerencia segun el dato introducido
	function sugerencias($string){
		$data = array();
		$Address = new AddressStandardizationSolution;
		$Output = $Address->AddressLineStandardization($string);
		array_push($data,$Output);
		return $data;
	}

	die(json_encode(sugerencias($_POST['string'])));
 ?>