<?php
/*
Plugin Name: Formulario
Plugin URI: http://google.com
Description: Formulario 
Version: 1.0
Author: Vanessa Rodríguez Silva
Author URI: https://vanessarodriguezsilva.wordpress.com
License: GLP2
*/

// include autoload.php
require dirname(__FILE__) . '/autoload.php';


// CSS ADD
	function vr_load_css() {
	    wp_register_style( 'custom_plugin_css', plugin_dir_url( __FILE__ ).'css/custom-style.css', false, '1.0.0');
	    wp_enqueue_style('custom_plugin_css');
	    
	    wp_register_style( 'jquery_plugin_css', plugin_dir_url( __FILE__ ).'css/jquery-ui.css', false, '1.11.4');
	    wp_enqueue_style('jquery_plugin_css');
	    
	    wp_register_style( 'bootstrap_plugin_css', plugin_dir_url( __FILE__ ).'css/bootstrap.min.css', false, null);
	    wp_enqueue_style('bootstrap_plugin_css');
	}
	add_action( 'admin_enqueue_scripts', 'vr_load_css' );
	add_action( 'wp_enqueue_scripts', 'vr_load_css' );

// JS ADD
	function vr_load_js() {
	    wp_register_script( 'custom_plugin_js', plugin_dir_url( __FILE__ ).'js/custom-script.js', false, '1.0.0');
	    wp_enqueue_script('custom_plugin_js');
	    
	    wp_register_script( 'jquery_plugin_js', 'https://code.jquery.com/jquery-1.12.4.js', false, '1.12.4');
	    wp_enqueue_script('jquery_plugin_js');
	    
	    wp_register_script( 'jqueryui_plugin_js', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js', false, '1.11.4');
	    wp_enqueue_script('jqueryui_plugin_js');
	    
	    wp_register_script( 'bootstrap_plugin_js', plugin_dir_url( __FILE__ ).'js/bootstrap.min.js', false, null);
	    wp_enqueue_script('bootstrap_plugin_js');
	}
	add_action( 'admin_enqueue_scripts', 'vr_load_js' );
	add_action( 'wp_enqueue_scripts', 'vr_load_js' );


// CONEXION DB
	function vr_conexion_db($conn){
		
		$servername = DB_HOST;
		$username = DB_USER;
		$password = DB_PASSWORD;
		$dbname = DB_NAME;

		if(strpos(DB_HOST, ':') !== false) {
			$db_host = explode(':', DB_HOST);
			$servername = $db_host[0];
			$port = $db_host[1];
		}

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname, $port);
		// Check connection
		if ($conn->connect_error) {
		    die("Conexión fallida: " . $conn->connect_error);
		} 

		return $conn;
	}

// CREACION DE TABLA EN LA BASE DE DATOS
	function vr_create_table_db(){
		
		// Inicio de conexiondb
		$conn = vr_conexion_db(0);
		
		// Sql to create table
		$sql = 'CREATE TABLE vr_form_plugin(
		id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
		firstname VARCHAR(30) NOT NULL,
		lastname VARCHAR(30) NOT NULL,
		idtype VARCHAR(30) NOT NULL,
		idnumber VARCHAR(20) NOT NULL,
		movil VARCHAR(20),
		email VARCHAR(50),
		adress VARCHAR(50),
		adressback VARCHAR(50),
		reg_date TIMESTAMP
		)';
		if ($conn->query($sql) === TRUE) {
		    echo "Se creo exitosamente la tabla vr_form_plugin en la base de datos.";
		} else {
		    echo "<p>".$conn->error."<br></p>";
		}

		// Cierre de la conexiondb
		$conn->close();
	}

// ADMIN FUNCTIONS 

// ADD PAGE IN ADMINISTRATOR ENVIROMENT
	add_action('admin_menu', 'test_plugin_setup_menu');
	function test_plugin_setup_menu(){
	    add_menu_page( 'Formulario Page', 'Formulario', 'manage_options', 'test-plugin', 'test_init' );
	}

// MAIN FUNCTION TO ADMINISTRATOR ENVIROMENT 
	function test_init(){
	    echo "<h1>Base de datos del lado del backend</h1>";

	    //creacion de la base de datos 
	    vr_create_table_db();

	    // imprimir tabla de registros en el ambiente de administrador
	    vr_show_table_back();
	}

// SHOW DATA BASE IN ADMINISTRATOR ENVIROMENT
	function vr_show_table_back(){
		
		// inicio de conexion db
		$conn = vr_conexion_db(0);

		// consulta de registros
		$sql = "SELECT * FROM vr_form_plugin";
		echo "Registros totales en la base de datos: ".$conn->query($sql)->num_rows;

		// filto por fecha de registro
		if($conn->query($sql)->num_rows >0){
		    echo '	<div class="container">
		    			<div class="col-xs-12">
			    			<p>
				    			Filtrar: <input type="text" id="datepicker">
				    			<button onclick="FilterByDate()">Filtrar</button>
				    			<button onclick="FilterByDateReset()">Limpiar</button>
			    			</p>
		    			</div>
		    		</div>
				  	<script>
					  	$( function() {
					    	$( "#datepicker" ).datepicker();
					  	} );
				  	</script>';
		}

		// sql show table
		$sql = "SELECT id, firstname, lastname, idtype, idnumber, movil, email, adressback, reg_date 
				FROM vr_form_plugin 
				ORDER BY reg_date";
		$result = $conn->query($sql);
		
		if ($result->num_rows > 0) {
		    // output data of each row
		    echo ' <table class="table table-striped" id="myTable">
		    	<thead>
		    		<tr>
		    			<th>ID</th>
		    			<th>Nombres</th>
		    			<th>Apellidos</th>
		    			<th>Tipo de doc.</th>
		    			<th>Documento</th>
		    			<th>Celular y/o fijo</th>
		    			<th>Email</th>
		    			<th>Dirección</th>
		    			<th>Fecha</th>
		    		</tr>
		    	</thead>
		    	<tbody>';
		    while($row = $result->fetch_assoc()) {
		    	echo "<tr>";
		        echo "<td>".$row["id"]."</td>";
		        echo "<td>".$row["firstname"]."</td>";
		        echo "<td>".$row["lastname"]."</td>";
		        echo "<td>".$row["idtype"]."</td>";
		        echo "<td>".$row["idnumber"]."</td>";
		        echo "<td>".$row["movil"]."</td>";
		        echo "<td>".$row["email"]."</td>";
		        echo "<td>".$row["adressback"]."</td>";
		        echo "<td>".$row["reg_date"]."</td>";
		        echo "</tr>";
		    }
		    echo "
		    	</tbody>
		    </table>";
		} else {
		    echo "<p class='col-xs-12'>0 Registros</p>";
		}
		// cierre de conexion db
		$conn->close();
	}

// CLIENT
	
// INSERCION DE DATOS DEL REGISTRO EXITOSO
	function vr_insertdata_db($firstname, $lastname, $idtipo, $id, $phone, $email, $address){
		
		// Estandarizacion de direcciones
		$Address = new AddressStandardizationSolution;
		 if (empty($_POST['direccion'])) {
		  	$Input = '';
		  } else {
		  	$Input = $_POST['direccion'];
		  }
		$Output = $Address->AddressLineStandardization($Input);

		// Inicio de conexion db
		$conn = vr_conexion_db(0);
		// insert register
		$sql = "INSERT INTO vr_form_plugin (firstname, lastname, idtype, idnumber, movil, email, adress, adressback, reg_date) ".
  			   "VALUES ('".$firstname."', '".$lastname."','".$idtipo."','".$id."','".$phone."', '".$email."','".$address."','".$Output."','".date("Y-m-d H:i:s")."')";
		if ($conn->query($sql) === TRUE) {
		    echo "Se registro exitosamente";
		} else {
		    echo "<p>Error: ".$conn->error."<br></p>";
		}
		// Cierre de conexion db
		$conn->close();
	}

	function html_form_code() {
		
		// define variables and set to empty values
		$firstnameErr = $lastnameErr = $emailErr = $phoneErr = $idErr = $idtipo = $addressErr = "";
		$firstname = $lastname = $email = $phone = $id = $address = $idtipoErr = "";

		// resive los datos por _POST[]
		if ($_SERVER["REQUEST_METHOD"] == "POST") {	

		  // NOMBRE
		  if (empty($_POST["nombre"])) {
		    $firstnameErr = " Campo requerido.";
		  } else {
		    $firstname = test_input($_POST["nombre"]);
		    // validacion de datos en el campo de NOMBRE
		    if (!preg_match("/[a-zA-Z\s]{1,40}$/",$firstname)) {
		      $firstnameErr = " Este campo solo admite letras."; 
		    }else{
		    	$complete++;
		    }
		  }

		  // APELLIDO
		  if (empty($_POST["apellido"])) {
		    $lastnameErr = " Campo requerido.";
		  } else {
		    $lastname = test_input($_POST["apellido"]);
		    // validacion de datos en el campo de APELLIDO
		    if (!preg_match("/[a-zA-Z\s]{1,50}$/",$lastname)) {
		      $lastnameErr = " Este campo solo admite letras"; 
		    }else{
		    	$complete++;
		    }
		  }
		  
		  // TIPO DE DOCUMENTO
		  if ($_POST["idtipo"] == "seleccionar") {
		    $idtipoErr = " Campo requerido.";
		  } else {
		    $idtipo = test_input($_POST["idtipo"]);
		    $complete++;
		  }

		  // DOCUEMTO
		  if(empty($_POST["id"])){
		    $idErr = " Campo requerido.";
		  } else {
		    $id = test_input($_POST["id"]);
		    // validacion de datos en el campo de DOCUMENTO
		    if (!preg_match("/[0-9]{1,20}$/",$id)) {
		      $idErr = " Formato no invalido. (recuerde no usar signos de puntuación)"; 
		    }else{
		    	$complete++;
		    }
		  }

		  // TELEFONO
		  if(empty($_POST["telefono"])){
		    $phoneErr = " Campo requerido.";
		  } else {
		    $phone = test_input($_POST["telefono"]);
		    // validacion de datos en el campo de TELEFONO
		    if (!preg_match("/[a-zA-Z0-9\s\:\-]{1,50}$/",$phone)) {
		      $phoneErr = " Formato no invalido. (recuerde no usar signos de puntuación a exepción de ':')"; 
		    }else{
		    	$complete++;
		    }
		  }

		  // EMAIL
		  if (empty($_POST["email"])) {
		    $emailErr = " Campo requerido.";
		  } else {
		    $email = test_input($_POST["email"]);
		    // validacion de datos en el campo de NOMBRE
		    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		      $emailErr = " Formato de email invalido."; 
		    }else{
		    	$complete++;
		    }
		  }

		  // DIRECCION
		  if(empty($_POST["direccion"])){
		    $addressErr = " Campo requerido.";
		  }else{
		    $address = test_input($_POST["direccion"]);
		    // validacion de datos en el campo de DIRECCION
		    if (!preg_match("/[a-zA-Z0-9\s\:\-\#\.\,]{1,50}$/",$address)) {
		      $addressErr = " Este campo solo admite letras y (#,.:)"; 
		    }else{
		    	$complete++;
		    }
		  }

		  // ENVIO DE DATOS PARA INSERCION EN DB
		  if($complete == 7){
			vr_insertdata_db($firstname, $lastname, $idtipo, $id, $phone, $email, $address);
			$complete = 0;
		  }

		}else{
			$complete=0;
		}

		// FORMULARIO DEL CLIENTE
		echo '	<form method="post" action="'.esc_url( $_SERVER['REQUEST_URI'] ).'">
					<div class="form-group row">
					  <label for="nombre-input" class="col-xs-12 col-md-6 col-form-label">Nombre</label>
					  <div class="col-xs-12 col-md-6">
					    <input class="form-control" type="text" id="nombre-input" name="nombre" value="'.$firstname.'">'.$firstnameErr.'
					  </div>
					</div>
					<div class="form-group row">
					  <label for="apellido-input" class="col-xs-12 col-md-6 col-form-label">Apellido</label>
					  <div class="col-xs-12 col-md-6">
					    <input class="form-control" type="search" id="apellido-input" name="apellido" value= "'.$lastname.'"> '.$lastnameErr.'
					  </div>
					</div>
					<div class="form-group row">
					  <label for="idtipo-input" class="col-xs-12 col-md-6 col-form-label">Tipo de documento</label>
					  <div class="col-xs-12 col-md-6">
					    <select class="form-control" id="idtipo-input" name="idtipo">
					      <option value="seleccionar">--Seleccionar--</option>
					      <option value="C.C.">Cedula de ciudadania</option>
					      <option value="C.E.">Cedula de extrangeria</option>
					      <option value="Pasaporte">Pasaporte</option>
					      <option value="T.I">Tarjeta de identidad</option>
					    </select>'.$idtipoErr.'
					  </div>
					</div>
					<div class="form-group row">
					  <label for="id-input" class="col-xs-12 col-md-6 col-form-label">Numero de Documento</label>
					  <div class="col-xs-12 col-md-6">
					    <input class="form-control" type="text" id="id-input" name="id" value="'.$id.'"> '.$idErr.'
					  </div>
					</div>
					<div class="form-group row">
					  <label for="tel-input" class="col-xs-12 col-md-6 col-form-label">Telefono movil y/o fijo</label>
					  <div class="col-xs-12 col-md-6">
					    <input class="form-control" type="tel" id="tel-input" name="telefono" value= "'.$phone.'"> '.$phoneErr.'
					  </div>
					</div>
					<div class="form-group row">
					  <label for="email-input" class="col-xs-12 col-md-6 col-form-label">Email</label>
					  <div class="col-xs-12 col-md-6">
					    <input class="form-control" type="email" id="email-input" name="email" value= "'.$email.'"> '.$emailErr.'
					  </div>
					</div>
					<div class="form-group row">
					  <label for="dir-input" class="col-xs-12 col-md-6 col-form-label">Dirección</label>
					  <div class="col-xs-12 col-md-6">
					    <input class="form-control" type="text" id="dir-input" name="direccion" value= "'.$address.'">'.$addressErr.'
					  </div>
					</div>
					<button type="submit" class="btn btn-primary col-xs-12 col-md-4 col-md-offset-8">Enviar</button>
				</form>';
				?>
				<!-- Ajax Autocomplete Direccion -->
				<script>
					$(function() {
						$( "#dir-input" ).autocomplete({
     						source: function( request, response ) {
			     				var address = jQuery('#dir-input').val();
								var data = {'string':address};    
								console.log(data);
					      		$.ajax({
			                        'type': 'POST',
			    					'dataType': 'json',
			    					'url': window.location.href+'wp-content/plugins/formulario/ajax.php',
			    					'data': data,
									 success: function( data ) {
										 response( $.map( data, function( item ) {
											return {
												label: item,
												value: item
											}
										}));
									}
					      		});
				      		},
					      	autoFocus: true,
					      	minLength: 0      	
			   			 });			    
					  });
				</script>Registros:
		<?php
		vr_show_table_front();
	}

	// validacion de datos 
	function test_input($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}

	// VISUALIZACION DE TABLA DE REGISTROS POR EL LADO DEL CLIENTE
	function vr_show_table_front(){
		
		// Inicio de conexion db
		$conn = vr_conexion_db(0);
		
		// sql show table
		$sql = "SELECT *
				FROM vr_form_plugin 
				ORDER BY reg_date";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
		    // output data of each row
		    echo ' <div class="card">';
		    while($row = $result->fetch_assoc()) {
		    	echo '<div class="col-xs-12 card-block">';
		        echo "<p> ID: ".$row["id"]."</p>";
		        echo "<p> Name: ".$row["firstname"]."</p>";
		        echo "<p> Apellidos: ".$row["lastname"]."</p>";
		        echo "<p> Doc:".$row["idtype"]."</p>";
		        echo "<p> T.Doc: ".$row["idnumber"]."</p>";
		        echo "<p> Tel: ".$row["movil"]."</p>";
		        echo "<p> Email: ".$row["email"]."</p>";
		        echo "<p> Dirección:".$row["adress"]."</p>";
		        echo "<p> Fecha Reg:".$row["reg_date"]."</p>";
		        echo "</div>";
		    }
		    echo "</div>";
		} else {
		    echo "<p>0 Registros</p>";
		}

		//Cierre coneccion db 
		$conn->close();
	}

// SHORTCODE TO ADMIN [formulario]
	add_shortcode('formulario', 'form_client');
	function form_client() {
		ob_start();
		html_form_code();
		return ob_get_clean();
	}
?>