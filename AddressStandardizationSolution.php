<?php

/**
 * Address Standardization Solution, PHP Edition.
 *
 * Requires PHP 5 or later.
 *
 * Address Standardization Solution is a trademark of The Analysis
 * and Solutions Company.
 *
 * @package AddressStandardizationSolution
 * @author Daniel Convissor <danielc@analysisandsolutions.com>
 * @copyright The Analysis and Solutions Company, 2001-2010
 * @license http://www.analysisandsolutions.com/software/license.htm Simple Public License
 * @link http://www.analysisandsolutions.com/software/addr/addr.htm
 */


/**
 * Formats a Delivery Address Line according to the United States Postal
 * Service's Addressing Standards
 *
 * The class also contains a state list generator for use in XHTML forms.
 *
 * @package AddressStandardizationSolution
 * @author Daniel Convissor <danielc@analysisandsolutions.com>
 * @copyright The Analysis and Solutions Company, 2001-2010
 * @license http://www.analysisandsolutions.com/software/license.htm Simple Public License
 * @link http://www.analysisandsolutions.com/software/addr/addr.htm
 */
class AddressStandardizationSolution {

	/**
	 * An array with compass directions as keys and abbreviations as values
	 *
	 * Note: entries ending in "-R" are for reverse lookup.
	 *
	 * @var array
	 */
	public $directionals = array(
		// custom Avd
		'ESTE' => 'ESTE',
		'NORTE' => 'NORTE',
		'OESTE' => 'OESTE',
		'SUR' => 'SUR',
		'AUTOPISTA' => 'AU',
		'AVENIDA' => 'AV',
		"AVENIDA CALLE"	=> "AC",
		"AVENIDA CARRERA"	=> "AK",
		"CALLE"	=> "CL",
		"CARRERA"	=> "KR",
		"CARRETERA"	=> "CT",
		"CIRCULAR"	=> "CQ",
		"CIRCUNVALAR"	=> "CV",
		"DIAGONAL"	=> "DG",
		"TRANSVERSAL"	=> "TV",
		"TRONCAL" =>	"TC",
		"VARIANTE" =>	"VT",
		"VIA" =>	"VI",
		"ADMINISTRACION" =>	"AD",
		"AGRUPACIÓN" =>	"AG",
		"APARTAMENTO" =>	"AP",
		"BARRIO" =>	"BR",
		"BLOQUE" =>	"BQ",
		"BODEGA" =>	"BG",
		"CASA" =>	"CS",
		"CENTRO COMERCIAL" =>	"CE",
		"CIUDADELA" =>	"CD",
		"MANZANA" =>	"MZ",
		"MODULO" =>	"MD",
		"OFICINA" =>	"OF",
		"PARQUE" =>	"PQ",
		"PARQUEADERO" =>	"PA",
		"PENT-HOUSE" =>	"PN",
		"PISO" =>	"PI",
		"PLANTA" =>	"PL",
		"PORTERIA" =>	"PR",
		"SUITE" =>	"ST",
		"SUPER MANZANA" =>	"SM",
		"TERRAZA" =>	"TZ",
		"TORRE" =>	"TO",
		"UNIDAD" =>	"UN",
		"UNIDAD RESIDENCIAL" =>	"UL",
		"URBANIZACIÓN" =>	"UR",
		"ZONA" =>	"ZN",
		"CONJUNTO RESIDENCIAL" =>	"CO",
		"CONSULTORIO" =>	"CN",
		"EDIFICIO" =>	"ED",
		"ENTRADA" =>	"EN",
		"ESQUINA" =>	"EQ",
		"ESTACION" =>	"ES",
		"ETAPA" =>	"ET",
		"EXTERIOR" =>	"EX",
		"FINCA" =>	"FI",
		"GARAJE" =>	"GA",
		"INTERIOR" =>	"IN",
		"KILOMETRO" =>	"KM",
		"LOCAL" =>	"LC",
		"LOTE" =>	"LT",
		"MEZZANINE" =>	"MN",
		"NO" => "",
		"NUM" => "",
		"N" =>"",
		"NUMERO" => "",
		"ANTIGUA" => "ANTIGUA",
		"NUEVA" => "NUEVA",

	);

	/**
	 * Formats a Delivery Address Line according to the United States Postal
	 * Service's Addressing Standards
	 *
	 * This comes in VERY handy when searching for records by address.
	 * Let's say a data entry person put an address in as
	 * "Two N Boulevard."  Later, someone else searches for them using
	 * "2 North Blvd."  Unfortunately, that query won't find them.  Such
	 * problems are averted by using this method before storing and
	 * searching for data.
	 *
	 * Standardization can also help obtain lower bulk mailing rates.
	 *
	 * Based upon USPS Publication 28, November 1997.
	 *
	 * @param string $address  the address to be converted
	 *
	 * @return string  the cleaned up address
	 *
	 * @link http://pe.usps.gov/cpim/ftp/pubs/Pub28/pub28.pdf
	 */
	public function AddressLineStandardization($address) {
		if (empty($address)) {
			return '';
		}

		/*
		 * General input sanitization.
		 */
		$b="";		
		// REPLACE NUEVA O ANTIGUA ADDRESS
		
		$address = strtoupper($address);

		if(strpos($address,"ANTIGUA") !== FALSE){
			$address = preg_replace('[A+N+T+I+G+U+A]', '', $address);
			$b = " /ANTIGUA";
		}

		if(strpos($address,"NUEVA") !== FALSE){
			$address = preg_replace('[N+U+E+V+A]', '', $address);
			$b = " /NUEVA";
		}
		// Replace bogus characters with spaces.
		$address = preg_replace('@[^A-Z0-9 /#.]@', ' ', $address);

		// Remove starting and ending spaces.
		$address = trim($address);

		// Remove periods from ends.
		$address = preg_replace('@\.$@', '', $address);

		// Add spaces around hash marks to simplify later processing. //REPACE NUM. NUM 
		$address = str_replace('#', '', $address);

		// Remove duplicate separators and spacing around separators,
		// simplifying the next few steps.
		$address = preg_replace('@ *([/.-])+ *@', '\\1', $address);

		// Remove dashes between numberic/non-numerics combinations
		// at ends of lines (for apartment numbers "32-D" -> "32D").
		$address = preg_replace('@(?<=[0-9])-(?=[^0-9]+$)@', '', $address);
		$address = preg_replace('@(?<=[^0-9])-(?=[0-9]+$)@', '', $address);

		// Replace remaining separators with spaces.
		$address = preg_replace('@(?<=[^0-9])[/.-](?=[^0-9])@', ' ', $address);
		$address = preg_replace('@(?<=[0-9])[/.-](?=[^0-9])@', ' ', $address);
		$address = preg_replace('@(?<=[^0-9])[/.-](?=[0-9])@', ' ', $address);

		// Remove duplilcate spaces.
		$address = preg_replace('@\s+@', ' ', $address);

		// Remove hash marks where possible.
		if (preg_match('@(.+ )([A-Z]+)( #)( .+)@', $address, $atom)) {
			if (isset($this->identifiers[$atom[2]])) {
				$address = "$atom[1]$atom[2]$atom[4]";
			}
		}

		$address = trim($address);

		if (!$address) {
			return '';
		}

		
		/*
		 * Check for special addresses.
		 */

		$rural_alternatives = 'RR|RFD ROUTE|RURAL ROUTE|RURAL RTE|RURAL RT|RURAL DELIVERY|RD ROUTE|RD RTE|RD RT';
		if (preg_match('@^(' . $rural_alternatives . ') ?([0-9]+)([A-Z #]+)([0-9A-Z]+)(.*)$@', $address, $atom)) {
			return "RR $atom[2] BOX $atom[4]";
		}
		if (preg_match('@^(BOX|BX)([ #]*)([0-9A-Z]+) (' . $rural_alternatives . ') ?([0-9]+)(.*)$@', $address, $atom)) {
			return "RR $atom[5] BOX $atom[3]";
		}

		if (preg_match('@^((((POST|P) ?(OFFICE|O) ?)?(BOX|BX|B) |(POST|P) ?(OFFICE|O) ?)|FIRM CALLER|CALLER|BIN|LOCKBOX|DRAWER)( ?(# )*)([0-9A-Z-]+)(.*)$@', $address, $atom)) {
			return "PO BOX $atom[11]";
		}

		$highway_alternatives = 'HIGHWAY|HIGHWY|HIWAY|HIWY|HWAY|HWY|HYGHWAY|HYWAY|HYWY';
		if (preg_match('@^([0-9A-Z.-]+ ?[0-9/]* ?)(.*)( CNTY| COUNTY) (' . $highway_alternatives . ')( NO | # | )?([0-9A-Z]+)(.*)$@', $address, $atom)) {
			if (isset($this->states[$atom[2]])) {
				$atom[2] = $this->states[$atom[2]];
			}
			if (isset($this->identifiers[$atom[6]])) {
				$atom[6] = $this->identifiers[$atom[6]];
				$atom[7] = str_replace(' #', '', $atom[7]);
				return "$atom[1]$atom[2] COUNTY HWY $atom[6]$atom[7]";
			}
			return "$atom[1]$atom[2] COUNTY HIGHWAY $atom[6]" . $this->getEolAbbr($atom[7]);
		}

		if (preg_match('@^([0-9A-Z.-]+ ?[0-9/]* ?)(.*)( CR |( CNTY| COUNTY) (ROAD|RD))( NO | # | )?([0-9A-Z]+)(.*)$@', $address, $atom)) {
			if (isset($this->states[$atom[2]])) {
				$atom[2] = $this->states[$atom[2]];
			}
			if (isset($this->identifiers[$atom[7]])) {
				$atom[7] = $this->identifiers[$atom[7]];
				$atom[8] = str_replace(' #', '', $atom[8]);
				return "$atom[1]$atom[2] COUNTY RD $atom[7]$atom[8]";
			}
			return "$atom[1]$atom[2] COUNTY ROAD $atom[7]" . $this->getEolAbbr($atom[8]);
		}

		if (preg_match('@^([0-9A-Z.-]+ ?[0-9/]* ?)(.*)( SR|( STATE| ST) (ROAD|RD))( NO | # | )?([0-9A-Z]+)(.*)$@', $address, $atom)) {
			if (isset($this->states[$atom[2]])) {
				$atom[2] = $this->states[$atom[2]];
			}
			if (isset($this->identifiers[$atom[7]])) {
				$atom[7] = $this->identifiers[$atom[7]];
				$atom[8] = str_replace(' #', '', $atom[8]);
				return "$atom[1]$atom[2] STATE RD $atom[7]$atom[8]";
			}
			return "$atom[1]$atom[2] STATE ROAD $atom[7]" . $this->getEolAbbr($atom[8]);
		}

		if (preg_match('@^([0-9A-Z.-]+ ?[0-9/]* ?)(.*)( STATE| ST) (ROUTE|RTE|RT)( NO | # | )?([0-9A-Z]+)(.*)$@', $address, $atom)) {
			if (isset($this->states[$atom[2]])) {
				$atom[2] = $this->states[$atom[2]];
			}
			if (isset($this->identifiers[$atom[6]])) {
				$atom[6] = $this->identifiers[$atom[6]];
				$atom[7] = str_replace(' #', '', $atom[7]);
				return "$atom[1]$atom[2] STATE RTE $atom[6]$atom[7]";
			}
			return "$atom[1]$atom[2] STATE ROUTE $atom[6]" . $this->getEolAbbr($atom[7]);
		}

		if (preg_match('@^([0-9A-Z.-]+ [0-9/]* ?)(INTERSTATE|INTRST|INT|I) ?(' . $highway_alternatives . '|H)? ?([0-9]+)(.*)$@', $address, $atom)) {
			$atom[5] = str_replace(' BYP ', ' BYPASS ', $atom[5]);
			return "$atom[1]INTERSTATE $atom[4]" . $this->getEolAbbr($atom[5]);
		}

		if (preg_match('@^([0-9A-Z.-]+ ?[0-9/]* ?)(.*)( STATE| ST) (' . $highway_alternatives . ')( NO | # | )?([0-9A-Z]+)(.*)$@', $address, $atom)) {
			if (isset($this->states[$atom[2]])) {
				$atom[2] = $this->states[$atom[2]];
			}
			if (isset($this->identifiers[$atom[6]])) {
				$atom[6] = $this->identifiers[$atom[6]];
				$atom[7] = str_replace(' #', '', $atom[7]);
				return "$atom[1]$atom[2] STATE HWY $atom[6]$atom[7]";
			}
			return "$atom[1]$atom[2] STATE HIGHWAY $atom[6]" . $this->getEolAbbr($atom[7]);
		}

		if (preg_match('@^([0-9A-Z.-]+ ?[0-9/]* ?)(.*)( US| U S|UNITED STATES) (' . $highway_alternatives . ')( NO | # | )?([0-9A-Z]+)(.*)$@', $address, $atom)) {
			if (isset($this->states[$atom[2]])) {
				$atom[2] = $this->states[$atom[2]];
			}
			if (isset($this->identifiers[$atom[6]])) {
				$atom[6] = $this->identifiers[$atom[6]];
				$atom[7] = str_replace(' #', '', $atom[7]);
				return "$atom[1]$atom[2] US HWY $atom[6]$atom[7]";
			}
			return "$atom[1]$atom[2] US HIGHWAY $atom[6]" . $this->getEolAbbr($atom[7]);
		}

		if (preg_match('@^((' . $highway_alternatives . '|H) ?(CONTRACT|C)|STAR) ?(ROUTE|RTE|RT|R)?( NO | # | )?([0-9]+) ?([A-Z]+)(.*)$@', $address, $atom)) {
			return "HC $atom[6] BOX" . $this->getEolAbbr($atom[8]);
		}

		if (preg_match('@^([0-9A-Z.-]+ [0-9/]* ?)(RANCH )(ROAD|RD)( NO | # | )?([0-9A-Z]+)(.*)$@', $address, $atom)) {
			if (isset($this->identifiers[$atom[5]])) {
				$atom[5] = $this->identifiers[$atom[5]];
				$atom[6] = str_replace(' #', '', $atom[6]);
				return "$atom[1]RANCH RD $atom[5]$atom[6]";
			}
			return "$atom[1]RANCH ROAD $atom[5]" . $this->getEolAbbr($atom[6]);
		}

		$address = preg_replace('@^([0-9A-Z.-]+) ([0-9][/][0-9])@', '\\1%\\2', $address);

		if (preg_match('@^([0-9A-Z/%.-]+ )(ROAD|RD)([A-Z #]+)([0-9A-Z]+)(.*)$@', $address, $atom)) {
			$atom[1] = str_replace('%', ' ', $atom[1]);
			return "$atom[1]ROAD $atom[4]" . $this->getEolAbbr($atom[5]);
		}

		if (preg_match('@^([0-9A-Z/%.-]+ )(ROUTE|RTE|RT)([A-Z #]+)([0-9A-Z]+)(.*)$@', $address, $atom)) {
			$atom[1] = str_replace('%', ' ', $atom[1]);
			return "$atom[1]ROUTE $atom[4]" . $this->getEolAbbr($atom[5]);
		}

		if (preg_match('@^([0-9A-Z/%.-]+ )(AVENUE|AVENU|AVNUE|AVEN|AVN|AVE|AV) ([A-Z]+)(.*)$@', $address, $atom)) {
			$atom[1] = str_replace('%', ' ', $atom[1]);
			return "$atom[1]AVENUE $atom[3]" . $this->getEolAbbr($atom[4]);
		}

		if (preg_match('@^([0-9A-Z/%.-]+ )(BOULEVARD|BOULV|BOUL|BLVD) ([A-Z]+)(.*)$@', $address, $atom)) {
			$atom[1] = str_replace('%', ' ', $atom[1]);
			return "$atom[1]BOULEVARD " . $this->getEolAbbr("$atom[3]$atom[4]");
		}


		/*
		 * Handle normal addresses.
		 */

		$parts = explode(' ', $address);
		$count = count($parts) - 1;
		$suff = 0;
		$id = 0;

		for ($counter = $count; $counter > -1; $counter--) {
			$out[$counter] = $parts[$counter];

			if (isset($this->suffixes[$parts[$counter]])) {
				if (!$suff) {
					// The first suffix (from the right).

					if (!empty($out[$counter+1]) && !empty($out[$counter+2])) {
						switch ($out[$counter+1] . ' ' . $out[$counter+2]) {
							case 'EAST W':
							case 'WEST E':
							case 'NORTH S':
							case 'SOUTH N':
								// Already set.
								break;
							default:
								$out[$counter] = $this->suffixes[$parts[$counter]];
						}
					} else {
						$out[$counter] = $this->suffixes[$parts[$counter]];
					}
					if ($counter == $count) {
						$id++;
					}

				} else {
					// A subsequent suffix, display as full word,
					// but could be a name (ie: LA, SAINT or VIA).

					if (isset($this->suffixSimiles[$parts[$counter]])
						&& !isset($this->suffixes[$out[$counter+1]]))
					{
						$out[$counter] = $this->suffixSimiles[$parts[$counter]];
					} else {
						$out[$counter] = $this->suffixes[$parts[$counter]];
						$out[$counter] = $this->suffixes["$out[$counter]-R"];
					}
				}

				$suff++;

			} elseif (isset($this->identifiers[$parts[$counter]])) {
				$out[$counter] = $this->identifiers[$parts[$counter]];
				if ($suff > 0) {
					$out[$counter] = $this->identifiers["$out[$counter]-R"];
				}
				$id++;

			} elseif (isset($this->directionals[$parts[$counter]])) {
				$prior = $counter - 1;
				$next = $counter + 1;
				if ($count >= $next
					&& isset($this->suffixes[$parts[$next]]))
				{
					$out[$counter] = $this->directionals[$parts[$counter]];
					if ($suff <= 1) {
						$out[$counter] = $this->directionals["$out[$counter]-R"];
					}

				} elseif ($counter > 2
					&& !empty($parts[$next])
					&& isset($this->directionals[$parts[$next]]))
				{
					// Already set.

				} elseif ($counter == 2
					&& isset($this->directionals[$parts[$prior]]))
				{
					// Already set.

				} else {
					$out[$counter] = $this->directionals[$parts[$counter]];
				}

				if ($counter == $count) {
					$id = 1;
				}
			} elseif (preg_match('@^[0-9]+$@', $parts[$counter])
					  && $counter > 0
					  && $counter < $count)
			{
				if ($suff) {
					switch (substr($parts[$counter], -2)) {
						case 11:
						case 12:
						case 13:
							$out[$counter] = $parts[$counter] . 'TH';
							break;

						default:
							switch (substr($parts[$counter], -1)) {
								case 1:
									$out[$counter] = $parts[$counter] . 'ST';
									break;
								case 2:
									$out[$counter] = $parts[$counter] . 'ND';
									break;
								case 3:
									$out[$counter] = $parts[$counter] . 'RD';
									break;
								default:
									$out[$counter] = $parts[$counter] . 'TH';
							}
					}
				}
			}
		}

		$out[0] = str_replace('%', ' ', $out[0]);

		ksort($out);
		return implode(' ', $out).$b;
	}

	/**
	 * Implement abbreviations for words at the ends of certain address lines
	 *
	 * @param string $string  the address fragments to be analyzed
	 *
	 * @return string  the cleaned up string
	 */
	private function getEolAbbr($string) {
		$suff = 0;
		$id = 0;

		$parts = explode(' ', $string);
		$count = count($parts) - 1;

		for ($counter = $count; $counter > -1; $counter--) {
			if (isset($this->suffixes[$parts[$counter]])) {
				if (!$suff) {
					$out[$counter] = $this->suffixes[$parts[$counter]];
					$suff++;
					if ($counter == $count) {
						$id = 1;
					}
				} else {
					$out[$counter] = $parts[$counter];
				}

			} elseif (isset($this->identifiers[$parts[$counter]])) {
				$out[$counter] = $this->identifiers[$parts[$counter]];
				$id = 1;

			} elseif (isset($this->directionals[$parts[$counter]])) {
				$out[$counter] = $this->directionals[$parts[$counter]];
				if ($counter == $count) {
					$id = 1;
				}

			} else {
				$out[$counter] = $parts[$counter];
			}
		}

		ksort($out);
		return implode(' ', $out);
	}

	/**
	 * Generates a XHTML option list of states
	 *
	 * @param mixed $default  string or array of values to be selected
	 * @param string $name  the name attribute for the form element
	 * @param string $visible  what users see in the list:
	 *                         <kbd>Word</kbd> (names)
	 *                         or <kbd>Abbr</kbd> (initials)
	 * @param string $value  values for the options:
	 *                       <kbd>Word</kbd> (names)
	 *                       or <kbd>Abbr</kbd> (initials)
	 * @param string $class  class attribute for the <select> element
	 * @param string $multiple  should multiple selections be permitted?
	 *                          <kbd>Y</kbd> or <kbd>N</kbd>.
	 * @param string $size  number of rows visible at one time.
	 *                      <kbd>0</kbd> sets no size attribute.
	 * @return void
	 */
	public function StateList($default = '', $name = 'StateID',
			$visible = 'Word', $value = 'Abbr', $class = '', $multiple = 'N',
			$size = '0')
	{
		// Validate input, just in case.
		$legit = array('Abbr', 'Word');
		if (!in_array($visible, $legit)) {
			$visible = 'Word';
		}
		if (!in_array($value, $legit)) {
			$value = 'Abbr';
		}

		if ($visible == 'Word') {
			ksort($this->states);
		} else {
			asort($this->states);
		}

		echo "\n\n<select ";

		if ($class) {
			echo 'class="' . $class . '" ';
		}

		if ($size) {
			echo 'size="' . $size . '" ';
		}

		if ($multiple == 'Y') {
			echo 'multiple name="' . $name . '[]">' . "\n";
			if (is_array($default)) {
				$default_clean = array();
				foreach ($default as $val) {
					if (is_string($val)) {
						$default_clean[] = strtoupper($val);
					}
				}
			} else {
				if (is_string($default)) {
					$default_clean = array(strtoupper($default));
				} else {
					$default_clean = array();
				}
			}

		} else {
			echo 'name="' . $name . '">' . "\n";
			if (is_array($default)) {
				$default_clean = array(strtoupper(current($default)));
			} else {
				if (is_string($default)) {
					$default_clean = array(strtoupper($default));
				} else {
					$default_clean = array();
				}
			}
		}

		foreach ($this->states as $Word => $Abbr) {
			echo ' <option value="' . $$value . '"';
			if (in_array($$value, $default_clean)) {
				echo ' selected';
			}
			echo '>' . $$visible . "</option>\n";
		}

		echo "</select>\n\n";
	}
}
