// A $( document ).ready() block.
// console.log( "ready!" );

    var input, filter, table, tr, fecha_table, fecha_input, yt, dt, mt, yi, di, mi;
    function FilterByDate(){
	  	input = document.getElementById("datepicker");
	  	fecha_input = input.value.split("/");
	  	if(fecha_input.length == 3){
	  		yi = fecha_input[2];
	  		mi = fecha_input[0];
	  		di = fecha_input[1];
		  	table = document.getElementById("myTable");
		  	tr = table.getElementsByTagName("tr");
		  	for (i = 1; i < tr.length; i++) {
		    	fecha_table = tr[i].children[8].innerText.split(" ")[0].split("-");
		    	yt = fecha_table[0];
		    	mt = fecha_table[1];
		    	dt = fecha_table[2];
		    	if ( yt == yi && dt == di && mi == mt) {
					tr[i].style.display = "";
		     	} else {
		     		tr[i].style.display = "none";
		    	}       
		  	}
		}
    }
    
    function FilterByDateReset(){
	  	input = document.getElementById("datepicker");
	  	input.value = "";
	  	table = document.getElementById("myTable");
	  	tr = table.getElementsByTagName("tr");
	  	for (i = 0; i < tr.length; i++) {
	      		tr[i].style.display = "";    
	    }
    }